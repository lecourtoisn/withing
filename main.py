import hashlib
import json
import os
import shutil
from datetime import datetime, timedelta, date

import numpy
import requests
import yaml
from flask import Flask, request, redirect, session
from matplotlib import pyplot, use

use('Agg')
from requests import get, Request
from sklearn import linear_model


class CustomFlask(Flask):
    def __init__(self, import_name, static_url_path=None, static_folder="static", static_host=None, host_matching=False,
                 subdomain_matching=False, template_folder="templates", instance_path=None,
                 instance_relative_config=False, root_path=None):
        super().__init__(import_name, static_url_path, static_folder, static_host, host_matching, subdomain_matching,
                         template_folder, instance_path, instance_relative_config, root_path)
        self.config_file = f'data/config_{os.getenv("WITHING_ENV", "dev")}.yml'
        with open(self.config_file) as f:
            data = yaml.safe_load(f)
            self.conf = data
        with open("data/creds.yml") as f:
            data = yaml.safe_load(f)
            self.creds = data
            self.secret_key = data.get("session_secret_key", "dev_secret_key")


app = CustomFlask(__name__, static_folder="plots")
URL = "https://wbsapi.withings.net/measure"


class NoDataException(Exception):
    def __init__(self, last_day, nb_days):
        self.nb_days = nb_days
        self.last_day = last_day


class OlderDataNeededError(Exception):
    pass


class InvalidTokenError(Exception):
    pass


def set_session(withings_access_response):
    access_token = withings_access_response.json()["body"]["access_token"]
    refresh_token = withings_access_response.json()["body"]["refresh_token"]
    user_id = withings_access_response.json()["body"]["userid"]
    session["tokens"] = {
        "access_token": access_token,
        "refresh_token": refresh_token
    }
    session["user_id"] = user_id


def refresh_token():
    result = requests.post("https://wbsapi.withings.net/v2/oauth2", data={
        "action": "requesttoken",
        "grant_type": "refresh_token",
        "client_id": app.creds["client_id"],
        "client_secret": app.creds["client_secret"],
        "refresh_token": session["tokens"]["refresh_token"],
    })

    set_session(result)

    return session["tokens"]["access_token"]


def connected():
    return "tokens" in session and "access_token" in session["tokens"]


def get_full_url(*args):
    return os.path.join(app.conf["app_root"], *args)


def get_cache_file_path():
    userid = session['user_id']
    return f"data/cache_{userid}.json"


def get_plot_foler_path():
    userid = session['user_id']
    return f"plots/{userid}"


def query_measures(start_date=None, end_date=None, last_update=None):
    def _query(query_params, token=None):
        try:
            if not token:
                token = session["tokens"]["access_token"]
            result = get(
                URL,
                params=query_params,
                headers={
                    "Authorization": "Bearer {}".format(token)
                })
            if result.json().get("status") == 401:
                raise InvalidTokenError
        except FileNotFoundError:
            raise InvalidTokenError
        return result

    if not ((start_date and end_date) or last_update):
        raise Exception("Bad arguments")

    params = {
        "action": "getmeas",
        "meastype": "1",
    }
    if last_update:
        params["lastupdate"] = round(last_update.timestamp())
    else:
        params["startdate"] = round(start_date.timestamp())
        params["enddate"] = round(end_date.timestamp())
    try:
        measures = _query(params)
    except InvalidTokenError:
        refresh_token()
        measures = _query(params)
    return {m["date"]: m["measures"][0]["value"] for m in measures.json()["body"]["measuregrps"]}


def save_cache(data, last_update, earliest_date):
    with open(get_cache_file_path(), "w+") as f:
        json.dump({
            "last_update": round(last_update.timestamp()),
            "earliest_date": round(earliest_date.timestamp()),
            "data": data
        }, f)


def read_cache():
    with open(get_cache_file_path()) as f:
        cache = json.load(f)
    last_update = datetime.fromtimestamp(cache["last_update"])
    earliest_date = datetime.fromtimestamp(cache["earliest_date"])
    weight_data = {float(date): weight for date, weight in cache["data"].items()}
    return {"last_update": last_update, "earliest_date": earliest_date, "data": weight_data}


def get_measures_from_cache(start_date, force_query):
    """
    Also updates cache. Cache must only be read/updated here
    """
    now = datetime.now()
    cache_updated = True
    earliest_date = None
    try:
        cache = read_cache()
        last_update = cache["last_update"]
        earliest_date = cache["earliest_date"]
        weight_data = cache["data"]
        if round(earliest_date.timestamp()) > round(start_date.timestamp()):
            earliest_date = start_date
            raise OlderDataNeededError
        elif force_query or now - last_update > timedelta(hours=2):
            new_weight = query_measures(last_update=last_update)
            weight_data = {**weight_data, **new_weight}
        else:
            cache_updated = False
    except (FileNotFoundError, OlderDataNeededError):
        weight_data = query_measures(last_update=start_date)
    if cache_updated:
        save_cache(weight_data, now, earliest_date or start_date)
    return {datetime.fromtimestamp(date): weight for date, weight in weight_data.items()}


def get_all_measures(end_date, nb_days, force_query):
    # nb_days -1 because I suppose that today counts, so if nb_days were equals to 1 I would expect only today's weight
    # thus a delta of 0, taken to midnight
    start_date = (end_date - timedelta(days=nb_days - 1)).replace(hour=0, minute=0, second=0, microsecond=0)
    weight_data = get_measures_from_cache(start_date, force_query)
    relevant_data = {date: weight for date, weight in weight_data.items() if
                     start_date.date() <= date.date() <= end_date.date()}
    return relevant_data


def fat_oracle(slope, intercept):
    christmas_day = datetime(day=25, month=12, year=datetime.now().year, hour=17).timestamp() / 86400
    try:
        target_weight_date = datetime.fromtimestamp(int(86400 * (app.conf["default_goal"] - intercept) / slope))
        target_weight_date_repr = target_weight_date.strftime("%d %b %Y")
    except Exception:
        target_weight_date_repr = int(86400 * (app.conf["default_goal"] - intercept) / slope)

    result = ["You'll reach {} kg the <b>{}</b>".format(app.conf["default_goal"], target_weight_date_repr),
              "On christmas day you'll weight {:.1f} kg".format(slope * christmas_day + intercept),
              "You {} {:.0f}g each day".format("lose" if slope < 0 else "gain", abs(1000 * slope))]
    return "<br \>".join(result)


def create_plot(x_points, y_points, *functions):
    if not os.path.exists(get_plot_foler_path()):
        os.mkdir(get_plot_foler_path())
    plot_hash = hashlib.md5(str(x_points + y_points).encode()).hexdigest()
    path = f"{get_plot_foler_path()}/{plot_hash}.png"
    if not os.path.exists(path):
        pyplot.figure()
        points = [[point[0] for point in axis_points] for axis_points in (x_points, y_points)]
        pyplot.plot(*points, "ko-")
        pyplot.yticks(numpy.arange(app.conf["default_goal"], 70, 0.4))
        pyplot.axis([points[0][0] - 7, points[0][-1] + 30, None, None])
        pyplot.grid(True)
        for slope, intercept, title in functions:
            x_vals = numpy.array(pyplot.xlim())
            y_vals = intercept + slope * x_vals
            pyplot.plot(x_vals, y_vals, label=title)
        pyplot.legend()
        pyplot.savefig(path, dpi=60, bbox_inches="tight")
        pyplot.close()
    return path


def previsions(last_day, nb_days, force_query):
    w_data = get_all_measures(last_day, nb_days, force_query)
    dx = []
    dy = []
    if not w_data:
        raise NoDataException(last_day, nb_days)
    for date in sorted(w_data.keys()):
        # Just to simulate that I weight myself same hour every day
        dtemp = date.replace(hour=8, minute=0, second=0, microsecond=0)
        dx.append([round(dtemp.timestamp()) / 86400])
        dy.append([w_data[date] / 1000])
    regr = linear_model.LinearRegression()

    regr.fit(dx, dy)
    li_slope = regr.coef_[0][0]
    li_intercept = regr.intercept_[0]
    last_date = dx[-1][0]
    first_date = dx[0][0]
    last_weight = dy[-1][0]
    first_weight = dy[0][0]
    try:
        avg_slope = (first_weight - last_weight) / (first_date - last_date)
    except ZeroDivisionError:
        raise NoDataException(last_day, nb_days)
    avg_intercept = last_weight - avg_slope * last_date

    plot_path = create_plot(
        dx, dy,
        (li_slope, li_intercept, "Linear"),
        (avg_slope, avg_intercept, "Average"))

    results = ['<div style="float: left; margin-right: 20px">Last {} days:'.format(nb_days),
               "Linear regression method:",
               fat_oracle(li_slope, li_intercept),
               "",
               "Average method:",
               fat_oracle(avg_slope, avg_intercept) + "</div>" +
               '<div><img src="{}" /></div>'.format(get_full_url(plot_path))]

    return "<br \>".join(results)


def parse_request_args():
    last_day_str = request.args.get("day", default=None)
    try:
        ranges_str = request.args["ranges"]
        ranges = [] if ranges_str == "" else [int(r) for r in ranges_str.split(",")]
    except KeyError:
        ranges = app.conf["default_ranges"]
    try:
        milestones_str = request.args["milestones"]
    except KeyError:
        milestones_str = ",".join(str(mstone) for mstone in app.conf["default_milestones"])
    milestones = [] if milestones_str == "" else [
        datetime(year=int(date[4:8]), month=int(date[2:4]), day=int(date[:2]))
        for date in milestones_str.split(",")]

    last_day = datetime.today() if last_day_str is None \
        else datetime(int(last_day_str[4:8]), int(last_day_str[2:4]), int(last_day_str[:2]))
    return last_day, ranges, milestones, True if request.args.get("force_query", "false").lower() == "true" else False


def format_params(last_day, ranges, milestones):
    fmt_day = last_day.strftime("%d%m%Y")
    fmt_ranges = "" if not ranges else ",".join(str(r) for r in ranges)
    fmt_milestones = "" if not milestones else ",".join(milestone.strftime("%d%m%Y") for milestone in milestones)
    uri = "day={}&ranges={}&milestones={}".format(fmt_day, fmt_ranges, fmt_milestones)
    return {
        "uri": uri,
        "day": fmt_day,
        "ranges": fmt_ranges,
        "milestones": fmt_milestones
    }


def connection():
    url = "https://account.withings.com/oauth2_user/authorize2"
    data = {
        "response_type": "code",
        "client_id": app.creds["client_id"],
        "state": "osef",
        "scope": "user.metrics",
        "redirect_uri": app.conf["redirect_url"],
    }
    req = Request('GET', url, data=data, params=data)
    return redirect(req.prepare().url)


def handle_code(code):
    url = "https://wbsapi.withings.net/v2/oauth2"
    response = requests.post(url, data={
        "action": "requesttoken",
        "grant_type": "authorization_code",
        "client_id": app.creds["client_id"],
        "client_secret": app.creds["client_secret"],
        "code": code,
        "redirect_uri": app.conf["redirect_url"]
    })

    set_session(response)

    return redirect(get_full_url())


@app.route("/")
def stat():
    code = request.args.get('code')
    if code:
        return handle_code(code)
    if not connected():
        return connection()
    html = []
    last_day, ranges, milestones, force_query = parse_request_args()
    days_url_bar = "<div style='text-align: center; margin-bottom: 16px'>"
    days_url_bar += "<a style='color: black; text-decoration: None; font-size: 100%' href={}>Log with another acccount</a><br />".format(
        get_full_url("logout"))
    max_range = min((date.today() - last_day.date()).days, 5)
    min_range = max_range - 10
    for delta_day in range(min_range, max_range + 1):
        targeted_day = last_day + timedelta(days=delta_day)

        font_weight = "normal" if delta_day != 0 else "bold"
        params = get_full_url(f'?{format_params(targeted_day, ranges, milestones)["uri"]}')
        fmt_targeter_day = targeted_day.strftime("%d %b")
        days_url_bar += f'<a style="font-weight: {font_weight}; color: black; text-decoration: None; padding: 0 5px; border: 1px solid black" href="{params}">{fmt_targeter_day}</a> '
    uri_params = format_params(last_day, ranges, milestones)

    days_url_bar += "<a style='color: black; text-decoration: None; font-size: 150%' href={}&force_query=true>↻</a>".format(
        get_full_url(f'?{uri_params["uri"]}'))
    days_url_bar += """
    <form method="post" action="{}" style="display: inline">
        <input type="hidden" name="ranges" value="{}" />
        <input type="hidden" name="milestones" value="{}" />
        <input type="submit" value="💾" style="display: inline; border: none; background-color: transparent; font-size: 150%; padding: 0; cursor: pointer" />
    </form>
    """.format(get_full_url("save"), uri_params["ranges"], uri_params["milestones"])

    days_url_bar += "</div>"
    html.append(days_url_bar)
    html.append("<table style='width: 100%'><tr>")
    for col in range(-2, 1):
        considered_last_day = last_day + timedelta(days=col)
        html.append("<td><p style='text-align: center'>{}</p>".format(considered_last_day.strftime("%d %b %Y")))
        for i in ranges + [(considered_last_day - ms).days + 1 for ms in milestones]:
            try:
                html.append(previsions(considered_last_day, i, force_query))
            except NoDataException as err:
                html.append(f"<p>No data for the last {err.nb_days} days since {err.last_day}</p>")
            html.append("<br \\><br \\>")
        html.append(
            '<a href="{}">Raw data</a></td>'.format(
                get_full_url(f'data?{format_params(considered_last_day, ranges, milestones)["uri"]}')))
    html.append("</tr></table>")
    return "".join(html)


@app.route("/logout")
def logout():
    session.pop("token", None)
    return connection()


@app.route("/data")
def data():
    last_day, ranges, milestones, force_query = parse_request_args()
    html = []
    for i in ranges:
        html.append("Data for last {} days:".format(i))
        measures = get_all_measures(last_day, i, force_query)
        for date in sorted(measures.keys()):
            weight = measures[date]
            html.append("{}: {:.3f}".format(date.strftime("%d %b %Y"), weight / 1000))
        html.append("<br \>")
    return "<br \>".join(html)


@app.route("/clean")
def clean():
    try:
        if os.path.exists(get_cache_file_path()):
            os.remove(get_cache_file_path())
        shutil.rmtree(get_plot_foler_path())
    except IOError:
        return "Could not delete something. Weird."
    return redirect(get_full_url())


@app.route("/save", methods=["POST"])
def save():
    try:
        app.conf["default_ranges"] = [int(day_range) for day_range in request.form["ranges"].split(",")]
    except KeyError:
        pass
    try:
        app.conf["default_milestones"] = [str(milestone) for milestone in request.form["milestones"].split(",")]
    except KeyError:
        pass

    with open(app.config_file, "w+") as config_file:
        yaml.safe_dump(app.conf, config_file)
    return redirect("{}".format(get_full_url(f'?{request.query_string.decode("utf-8")}')))


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5050)
